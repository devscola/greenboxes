process.env.NODE_ENV = 'test'

const api = require('../../api');
const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const ProductorService = require('../../services/productors/service');

chai.use(chaiHttp);
let insertedProductor = {};

describe('Productors Controller', () => {
  const DEFAULT_NAME = 'John Doe';
  const productor = {
    name: DEFAULT_NAME,
    shortDesc: 'Blabla',
    longDesc: 'Blablabla',
    email: 'email@example.com',
    phone: '666 666 666',
    points: [
      {place: 'La Cova'},
      {place: 'El terra'}
    ]
  };
  it('is able to store new productors', (done) => {
    const SUCCESS_MESSAGE = 'Tu perfil ha sido creado con éxito.';

    chai.request(api)
      .post('/productor/create')
      .send(productor)
      .end((_, response) => {
        const res = JSON.parse(response.text);

        expect(res.productor.name).to.eq(DEFAULT_NAME);
        expect(res.message).to.eq(SUCCESS_MESSAGE);
        done();
      });
  });

  it('is able to update productors', (done) => {
    const updatedName = 'Jhon Doe updated!!';
    const SUCCESS_MESSAGE = 'Tu perfil ha sido actualizado.';

    insertOne().then(() => {
      const insertedId = insertedProductor._id;
      insertedProductor.name = updatedName;
      chai.request(api)
      .post('/productor/update')
      .send(insertedProductor)
      .end((_, response) => {
        const result = JSON.parse(response.text);
        expect(result.productor.name).to.eq(updatedName);
        expect(result.productor._id).to.eq(insertedId.toString());
        expect(result.message).to.eq(SUCCESS_MESSAGE);
        done();
      });
    });
  });

  it('is able to retrieve productor info', (done) => {
    insertOne().then(() => {
      const insertedId = insertedProductor._id;
      chai.request(api)
      .post('/productor/retrieve')
      .send({id: insertedId})
      .end((_, response) => {
        const result = JSON.parse(response.text);
        expect(result.productor.name).to.eq(DEFAULT_NAME);
        expect(result.productor._id).to.eq(insertedId.toString());
        done();
      });
    });
  });
  it('is able to retrieve all productors', (done) => {
    insertOne().then(() => {
      chai.request(api)
      .post('/productor/retrieve-all')
      .send()
      .end((_, response) => {
        const result = JSON.parse(response.text);
        expect(result.productors[0].name).to.eq(DEFAULT_NAME);
        expect(result.productors.length > 0).to.eq(true);
        done();
      });
    });
  });

  it('is able to retrieve productor info', (done) => {
    DELETE_MESSAGE = 'Tu cuenta de productor ha sido anulada.';
    insertOne().then(() => {
      const insertedId = insertedProductor._id;
      chai.request(api)
      .post('/productor/destroy')
      .send({id: insertedId})
      .end((_, response) => {
        const result = JSON.parse(response.text);
        expect(result.message).to.eq(DELETE_MESSAGE);
        done();
      });
    });
  });

  it('is able to update distribution points', (done) => {
    const SUCCESS_MESSAGE = 'El punto de reparto ha sido actualizado.';
    const UPDATED_PLACE = 'Canet';

    insertOne().then(() => {
      let point = insertedProductor.distribution[1];
      point.place = UPDATED_PLACE;
      chai.request(api)
      .post('/productor/distribution/update')
      .send(point)
      .end((_, response) => {
        const result = JSON.parse(response.text);
        expect(result.productor.name).to.eq(DEFAULT_NAME);
        expect(result.productor.distribution.length).to.eq(2);
        expect(result.productor.distribution[1].place).to.eq(UPDATED_PLACE);
        expect(result.message).to.eq(SUCCESS_MESSAGE);
        done();
      });
    });
  });

  insertOne = async () => {
    const response = await ProductorService.create(productor);
    expect(response.name).to.equal(productor.name);
    insertedProductor = response;
  };
});
