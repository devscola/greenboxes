process.env.NODE_ENV = 'test'
const expect = require('chai').expect;
const chai = require('chai');
const ProductorService = require('../../services/productors/service');


describe('Productors Service', () => {
  const DEFAULT_NAME = 'John Doe';
  let productor = {};
  beforeEach(() =>{
    productor = {
      name: DEFAULT_NAME,
      shortDesc: 'Blabla',
      longDesc: 'Blablabla',
      email: 'email@example.com',
      phone: '666 666 666',
      points: [
        {
          inPlace: true,
          place: 'La Cova',
          direction: 'Benimaclet',
          days: [{
            day: 'Lunes',
            hour: '12:00'
          }],
          tags:["Beni", "La Cova"]
        },
        {place: 'El terra'}
      ]
    };
  });
  it('is able to store new productors', async () => {
    const response = await ProductorService.create(productor);

    expect(response.name).to.equal(DEFAULT_NAME);
    expect(response.distribution.length).to.equal(2);
  });
  it('is able to edit productor', async () => {
    const updatedName = 'Jhon Doe updated!!';
    const response = await ProductorService.create(productor);
    expect(response.name).to.equal(DEFAULT_NAME);
    const insertedId = response._id;

    productor.name = updatedName;
    const updated_response = await ProductorService.update(productor, insertedId);

    expect(updated_response.name).to.equal(updatedName);
    expect(updated_response._id).to.eql(insertedId);
    expect(updated_response.distribution.length).to.equal(2);

  });
  it('is able to retrieve productor', async () => {
    const response = await ProductorService.create(productor);
    expect(response.name).to.equal(DEFAULT_NAME);
    const insertedId = response._id;

    const retrieved = await ProductorService.retrieve(insertedId);

    expect(retrieved.name).to.equal(DEFAULT_NAME);
    expect(retrieved._id).to.eql(insertedId);
    expect(retrieved.distribution.length).to.equal(2);

  });

  it('is able to retrieve all productors', async () => {
    const response = await ProductorService.create(productor);
    expect(response.name).to.equal(DEFAULT_NAME);

    const retrieved = await ProductorService.retrieveAll();

    expect(retrieved.length > 0).to.equal(true);
    expect(retrieved.slice(-1)[0].name).to.equal(DEFAULT_NAME);
    expect(retrieved.slice(-1)[0].distribution.length).to.equal(2);
  });

  it('is able to delete a productor', async () => {
    const response = await ProductorService.create(productor);
    expect(response.name).to.equal(DEFAULT_NAME);
    const insertedId = response._id;

    const result = await ProductorService.destroy(insertedId);
    expect(result).to.equal(true);
  });

  it('is able to update distribution point', async () => {
    const newPoint = {place: 'Canet'};
    const response = await ProductorService.create(productor);
    expect(response.name).to.equal(DEFAULT_NAME);
    const distributionId = response.distribution[1]._id;
    const insertedId = response._id;
    newPoint._id = distributionId;
    newPoint.inPlace = false;
    newPoint.place = "La Cova Updated";
    newPoint.direction = "Benimaclet Updated";
    newPoint.days = [{day: 'Martes', hour: "13:00"}];
    newPoint.tags = ["La Cova", "Benimaclet"];

    const updated_response = await ProductorService.updateDistribution(newPoint);

    expect(updated_response.distribution.length).to.equal(2);
    expect(updated_response.distribution[0].place).to.equal("La Cova");
    expect(updated_response.distribution[1].inPlace).to.equal(false);
    expect(updated_response.distribution[1].direction).to.equal("Benimaclet Updated");
    expect(updated_response.distribution[1].days).to.eql([{day: 'Martes', hour: "13:00"}]);
    expect(updated_response.distribution[1].tags).to.eql(["La Cova", "Benimaclet"]);
  });
});
