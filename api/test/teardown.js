let abortNeeded = false;
before(async () => {
  if(process.env.NODE_ENV  == 'test'){
    abortNeeded = true;
  }
});
after(async () => {
  if(abortNeeded){
    process.exit();
  }
});
