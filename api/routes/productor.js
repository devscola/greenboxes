const Actions = require('../actions/productor');
const express = require('express');

const router = express.Router();

router.post('/create', function (req, res, next) {
  const data = req.body;
  let response = {};
  Actions.create.invoke(data).then(result => {
    response.message = 'Tu perfil ha sido creado con éxito.';
    response.productor = result;

    res.status(201).json(response);
  }).catch(error => next(error));
});

router.post('/update', function (req, res, next) {
  const data = req.body;
  let response = {};
  Actions.update.invoke(data).then(result => {
    response.message = 'Tu perfil ha sido actualizado.';
    response.productor = result;

    res.status(200).json(response);
  }).catch(error => next(error));
});

router.post('/retrieve', function (req, res, next) {
  const data = req.body;
  let response = {};
  Actions.retrieve.invoke(data.id).then(result => {
    response.productor = result;

    res.status(200).json(response);
  }).catch(error => next(error));
});

router.post('/retrieve-all', function (req, res, next) {
  let response = {};
  Actions.retrieveAll.invoke().then(result => {
    response.productors = result;

    res.status(200).json(response);
  }).catch(error => next(error));
});

router.post('/distribution/update', function (req, res, next) {
  const data = req.body;
  let response = {};
  Actions.updateDistribution.invoke(data).then(result => {
    response.message = 'El punto de reparto ha sido actualizado.';
    response.productor = result;

    res.status(200).json(response);
  }).catch(error => next(error));
});

router.post('/destroy', function (req, res) {
  SUCCESS_MESSAGE = 'Tu cuenta de productor ha sido anulada.';
  ERROR_MESSAGE = 'Ha habido un error anulando tu cuenta.';
  const data = req.body;
  let response = {};
  Actions.destroy.invoke(data.id).then(result => {
    response.message = (result) ? SUCCESS_MESSAGE : ERROR_MESSAGE;

    res.status(200).json(response);
  });
});

module.exports = router;
