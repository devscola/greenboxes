const mongodb = require('mongodb');
const getDB = require('../../utils/database').getDB;

class Collection {
  static async create (productor) {
    const db = await getDB();

    return db.collection('productors')
      .insertOne(productor)
      .then( result => {
        return Collection._insertedObject(result);
      })
      .catch(e => {
        throw e;
      });
  }

  static async retrieve (id) {
    const db = await getDB();

    return db.collection('productors')
      .findOne({_id: new mongodb.ObjectID(id)})
      .then( result => {
        return result;
      })
      .catch(e => {
        throw e;
      });
  }

  static async retrieveAll () {
    const db = await getDB();

    return db.collection('productors')
      .find({})
      .toArray()
      .then( result => {
        return result;
      })
      .catch(e => {
        throw e;
      });
  }

  static async update (productor, id) {
    const db = await getDB();
    return db.collection('productors')
    .findOneAndUpdate({_id: new mongodb.ObjectID(id)}, {$set:{
      name: productor.name,
      shortDesc: productor.shortDesc,
      longDesc: productor.longDesc,
      email: productor.email,
      phone: productor.phone,
    }},{returnOriginal:false})
    .then( result => {
      return result.value;
    })
    .catch(e => {
      throw e;
    });
  }

  static async updateDistribution (point) {
    const db = await getDB();
    return db.collection('productors')
    .findOneAndUpdate({"distribution._id": new mongodb.ObjectID(point._id)}, {$set:{
      "distribution.$": point,
    }},{returnOriginal:false})
    .then( result => {
      return result.value;
    })
    .catch(e => {
      throw e;
    });
  }

  static async destroy (id) {
    const db = await getDB();

    return db.collection('productors')
    .deleteOne({_id: new mongodb.ObjectID(id)})
    .then( result => {
      result = result.deletedCount === 1 ? true : false;
      return result;
    })
    .catch(e => {
      throw e;
    });
  }

  static _insertedObject(result){
    return result.ops[0];
  }
}


module.exports = Collection;
