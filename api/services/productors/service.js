const Collection = require('./collection');
const Productor = require('../../domain/Productor');
const Distribution = require('../../domain/Distribution');


class Service {
  static create (productorData) {
    try {
      const productor = new Productor(
        productorData.name, productorData.shortDesc, productorData.longDesc,
        productorData.email, productorData.phone, productorData.points
      );
      return Collection.create(productor)
        .then(result => result)
        .catch(error => {
          throw error
        })
    } catch (error) {
      const message = 'Error creating new productor'
    }
  }
  static update (productorData, id) {
    try {
    const productor = new Productor(
      productorData.name, productorData.shortDesc, productorData.longDesc, productorData.email, productorData.phone
    );
    return Collection.update(productor, id)
      .then(result => result)
      .catch(error => {
        throw error
      })
    } catch (error) {
      const message = 'Error updating productor'
    }
  }

  static retrieve (id) {
    return Collection.retrieve(id)
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  static retrieveAll () {
    return Collection.retrieveAll()
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  static destroy (id) {
    return Collection.destroy(id)
      .then(deleted => deleted)
      .catch(error => {
        throw error
      })
  }

  static updateDistribution(distribution){
    try {
      const point = new Distribution(
        distribution.inPlace, distribution.place, distribution.direction,
        distribution.days, distribution.tags, distribution._id);
      return Collection.updateDistribution(point)
        .then(result => result)
        .catch(error => {
          throw error
        })
    } catch (error) {
      const message = 'Error updating distribution point'
    }
  }
}

module.exports = Service;
