const Collection = require('./collection')
const Service = require('./service')

const Productor = {
  service: Service,
  collection: Collection
}

module.exports = Productor
