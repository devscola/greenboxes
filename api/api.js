const express = require('express');
const cors = require('cors')
require('./utils/environment').loadEnvVars()
const { logger } = require('./utils/logger')
const uuidv4 = require('uuid/v4')
const productorRoutes = require('./routes/productor');
const api = express();
const Sentry = require('@sentry/node')
Sentry.init({ dsn: 'https://e768b378610e47a0819c1193b73c1acc@sentry.io/1526658' });

api.use(Sentry.Handlers.requestHandler());
api.use(cors())
api.options('*', cors())

api.use(express.json());
api.use(express.urlencoded({ extended: false }));
api.use((req, res, next) => {
  req.logId = uuidv4()
  logger.info('', {
    method: req.method,
    path: req.url,
    logId: req.logId,
    msg: `${req.method} ${req.url}`
  })
  next()
})

api.use('/productor', productorRoutes);
api.use(Sentry.Handlers.errorHandler());

api.use((err, req, res, next) => {
  logger.error('', {
    error: err.error || {},
    message: err.message,
    logId: req.logId,
    meta: err
  })

  let error = {}
  error.error = err.error
  error.statusCode = err.statusCode || 500
  error.message = err.message || 'Sorry, we have an unexpected error'
  error.type = err.type || 'SERVER_ERROR'
  error.meta = err.meta || {}
  res.status(error.statusCode).json(error)
  res.end(res.sentry + "\n");
})

module.exports = api;
