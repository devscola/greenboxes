const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db = null;

const connection = () => {
  if(process.env.NODE_ENV=="test"){
    console.log('In testing mode');
    return MongoClient.connect(
      'mongodb://mongo:27017/test',
      { useNewUrlParser: true }
    )
    .then((client) => {
      _db = client.db();
      console.log('MongoDB Connected');
      return _db;
    })
    .catch(err => console.log("errorErrorError"));
  }else{
    let url = {};
    if(process.env.NODE_ENV == 'production'){
      console.log('In production mode');
      url = `mongodb+srv://${process.env.STAGING_DB_USER}:${process.env.STAGING_DB_PASS}@laalternativa-egb2b.mongodb.net/${process.env.STAGING_DB_NAME}?retryWrites=truew=majority`;

    }else {
      console.log('In development mode');
      url = `mongodb+srv://${process.env.STAGING_DB_USER}:${process.env.STAGING_DB_PASS}@laalternativa-egb2b.mongodb.net/${process.env.STAGING_DB_NAME}?retryWrites=truew=majority`;
    }
    return MongoClient.connect(url, { useNewUrlParser: true }).then(client => {
      _db = client.db();
      return _db;
    });
  }
};

const getDB = () => {
  if (_db) {
    return _db;
  }else {
    return connection();
  }
  throw 'No database found!';
};

exports.getDB = async () => {
  return await getDB();
};
