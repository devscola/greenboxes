const badRequest = (error, message) => {
  const err = {}
  err.error = {}
  err.error.code = error.code || 0
  err.error.name = error.mane || ''
  err.error.message = error.message || ''
  err.message = message
  err.meta = error
  err.statusCode = 400
  err.type = 'BAD_REQUEST'
  return err
}
const errors = {
  badRequest: badRequest
}

module.exports = errors
