const assert = require('assert')
const path = require('path')

const checkEnvVars = () => {
  try {
    assert.ok(process.env.NODE_ENV)
    assert.ok(process.env.PORT)
    assert.ok(process.env.STAGING_DB_USER)
    assert.ok(process.env.STAGING_DB_NAME)
    assert.ok(process.env.STAGING_DB_PASS)
  } catch (err) {
    throw new Error(`Set the Environment Variables needed!\n${err}`)
  }
}

const loadEnvVars = () => {
  if (process.env.NODE_ENV) {
    require('dotenv').config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}.local`) })
    require('dotenv').config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`) })
  }
  require('dotenv').config({ path: path.resolve(process.cwd(), '.env.local') })
  require('dotenv').config({ path: path.resolve(process.cwd(), '.env') })

  checkEnvVars()
}


module.exports = {
  checkEnvVars,
  loadEnvVars
}
