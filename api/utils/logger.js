const { transports } = require('winston')
const winston = require('winston')
const pkg = require('../package.json')

const levels = {
  error: 0,
  warn: 1,
  info: 2
}

const addAppInfoFormat = winston.format((info, opts) => {
  info.app = opts.name || pkg.name
  info.version = opts.version || pkg.version
  return info
})

let consoleTransport = new winston.transports.Console({ level: process.env.LOGS_LEVEL })

let logger = winston.createLogger({
  level: levels,
  format: winston.format.combine(
    winston.format.timestamp(),
    addAppInfoFormat(),
    winston.format.json()
  ),
  transports: [
    consoleTransport,
    new winston.transports.File({ filename: './error.log', level: 'error', json: true }),
    new winston.transports.File({ filename: './combined.log', level: process.env.LOGS_LEVEL, json: true })
  ]
})

if (process.env.NODE_ENV === 'test') {
  logger = winston.createLogger({
    level: levels,
    format: winston.format.combine(
      winston.format.timestamp(),
      addAppInfoFormat(),
      winston.format.json()
    ),
    transports: [
      new winston.transports.File({ filename: './error.log', level: 'error', json: true }),
      new winston.transports.File({ filename: './combined.log', level: process.env.LOGS_LEVEL, json: true })
    ]
  })
}

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }))
}

module.exports = {
  logger: logger
}
