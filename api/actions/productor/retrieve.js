const Productor = require('../../services/productors');

class Retrieve {
  static invoke(id) {
    return Productor.service.retrieve(id)
            .then(result => result)
            .catch(error => {
              throw error
            })
  }
}

module.exports = Retrieve;
