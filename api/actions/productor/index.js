const Create = require('./create');
const Update = require('./update');
const Retrieve = require('./retrieve');
const RetrieveAll = require('./retrieveAll');
const Destroy = require('./destroy');
const UpdateDistribution = require('./updateDistribution');

const Actions = {
  create: Create,
  update: Update,
  retrieve: Retrieve,
  retrieveAll: RetrieveAll,
  destroy: Destroy,
  updateDistribution: UpdateDistribution,
};

module.exports = Actions;
