const Productor = require('../../services/productors');

class RetrieveAll {
  static invoke() {
    return Productor.service.retrieveAll()
            .then(result => result)
            .catch(error => {
              throw error
            })
  }
}

module.exports = RetrieveAll;
