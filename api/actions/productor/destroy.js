const Productor = require('../../services/productors');

class Destroy {
  static invoke(id) {
    return Productor.service.destroy(id)
            .then(result => result)
            .catch(error => {
              throw error
            })
  }
}

module.exports = Destroy;
