const Productor = require('../../services/productors');

class UpdateDistribution {
  static invoke(pointData) {
    return Productor.service.updateDistribution(pointData)
            .then(result => result)
            .catch(error => {
              throw error
            })
  }
}

module.exports = UpdateDistribution;
