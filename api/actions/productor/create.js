const Productor = require('../../services/productors')

class Create {
  static invoke(productorData) {
    return Productor.service.create(productorData)
            .then(result => {
              return result
            })
            .catch(error => {
              throw error
            })
  }
}

module.exports = Create
