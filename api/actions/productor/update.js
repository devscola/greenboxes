const Productor = require('../../services/productors');

class Update {
  static invoke(productorData) {
    return Productor.service.update(productorData, productorData._id)
            .then(result => result)
            .catch(error => {
              throw error
            })
  }
}

module.exports = Update;
