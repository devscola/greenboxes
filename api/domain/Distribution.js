const mongodb = require('mongodb');

class Distribution {
  constructor (inPlace, place, direction, days, tags, id = null) {
    this._id = id == null ? new mongodb.ObjectID() : new mongodb.ObjectID(id);
    this.inPlace = inPlace;
    this.place = place;
    this.direction = direction;
    this.days = days;
    this.tags = tags;
  }
}



module.exports = Distribution;
