const mongodb = require('mongodb');

class Productor {
  constructor (name, shortDesc, longDesc, email, phone, distribution = null) {
    this.name = name;
    this.shortDesc = shortDesc;
    this.longDesc = longDesc;
    this.email = email;
    this.phone = phone;
    this.distribution = this._generateIds(distribution);
  }

  _generateIds(distribution){
    if(distribution !== null){
      distribution.map((item) => {
        item._id = new mongodb.ObjectID();
      });
    }
    return distribution;
  }
}



module.exports = Productor;
