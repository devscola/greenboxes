# Run App

1. Copy env file example to .env file add add your values of mongo atlas

`cp api/.env.example api/.env`

2. Use docker-compose in root path

`docker-compose run --rm api npm install`

`docker-compose up`

3. Run test

`docker-compose exec api npm test`

4. Open App

Open html files in your browser
