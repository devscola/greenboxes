class ProductorService extends Service {
  constructor(bus) {
    super();
    this.bus = bus;
    this._subscriptions();
  }
  _subscriptions() {
    this.bus.subscribe('productor.create', this._create.bind(this));
    this.bus.subscribe('productor.retrieve', this._retrieve.bind(this));
    this.bus.subscribe('productor.retrieve.all', this._retrieveAll.bind(this));
    this.bus.subscribe('productor.update', this._update.bind(this));
    this.bus.subscribe('productor.distribution.update', this._updateDistribution.bind(this));
    this.bus.subscribe('productor.destroy', this._destroy.bind(this));
  }

  _create(payload) {
    let callback = this.buildCallback('productor.stored');
    let body = payload;
    let url = '/productor/create';
    Client.doRequest(url, body, callback);
  }

  _update(payload) {
    let callback = this.buildCallback('productor.stored');
    let body = payload;
    let url = '/productor/update';
    Client.doRequest(url, body, callback);
  }

  _updateDistribution(payload) {
    let callback = this.buildCallback('productor.distribution.updated');
    let body = payload;
    let url = '/productor/distribution/update';
    Client.doRequest(url, body, callback);
  }

  _retrieve(payload) {
    let callback = this.buildCallback(payload.action);
    let body = payload;
    let url = '/productor/retrieve';
    Client.doRequest(url, body, callback);
  }

  _retrieveAll(payload) {
    let callback = this.buildCallback('productor.retrieved.all');
    let body = payload;
    let url = '/productor/retrieve-all';
    Client.doRequest(url, body, callback);
  }

  _destroy(payload) {
    let callback = this.buildCallback('productor.destroied');
    let body = payload;
    let url = '/productor/destroy';
    Client.doRequest(url, body, callback);
  }
}
