class CreateDistributionPoints extends PointForm{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._addedWebcomponents();
    this._template();
    this.points = [];
  }
  connectedCallback(){
    this._eventsListener();
    this._initialize();
  }

  collectData(event) {
    if(event !== undefined) event.preventDefault();
    this._collectCommonData();
    this._addPoint();
  }

  isPreparedForSave(){
    return this.inPlace.checked ||
    (this.direction.value.length > 0 && this._daysChecked());
  }

  _submitButton () {
    return '<button>Añadir punto de reparto</button>'
  }
  _titleContent() {
    return '<h2>Añade puntos de reparto</h2>'
  }
  _addPoint() {
    this._addPlace();
    this.points.push(this.data);
    this._clearAll();
  }

  _clearAll() {
    this.shadowRoot.querySelector('alternativa-distribution-tags').setAttribute('collect', false);
    this.clear();
    this._cleanForm();
  }

  _addPlace() {
    let element = document.createElement('li');
    element.textContent = this.data.place;
    this.shadowRoot.querySelector('ul').append(element);
  }

  _cleanForm() {
    this.place.value = '';
    this.direction.value = '';
    this.inPlace.checked = false;
    this._clearDays();
  }

  _clearDays() {
    this.shadowRoot.querySelectorAll('.days input').forEach((element) => {
      element.checked = false;
      element.parentNode.children[0].disabled = true
      element.parentNode.children[0].value = ''
    });
  }

  _daysChecked() {
    let checked = false;
    this.shadowRoot.querySelectorAll('.days input').forEach((element) => {
      if(element.checked) checked = true;
    });
    return checked;
  }

}

customElements.define('points-create', CreateDistributionPoints );
