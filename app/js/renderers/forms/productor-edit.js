class EditProductorForm extends ProductorForm{
  static get observedAttributes() { return ['productor']; }
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._addedWebcomponents();
  }
  connectedCallback(){
    const splitedURL = window.location.href.split("#");
    this.id = splitedURL[1];
    this._getProductor();
  }
  attributeChangedCallback(name, oldValue, newValue) {
    if(newValue != ""){
      this._startWebComponent();
      this._fillForm(JSON.parse(newValue));
    }
  }
  _startWebComponent() {
    this._template();
    this._initialize();
    this._eventsListener();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      form{
        max-width: 600px;
        margin: 0 auto;
        padding: 1em;
        background-color: #3c9296;
      }
      h2{
        font-size: 2em;
        color: #fff;
        border-bottom: 1px solid #fff;
      }
      label{color: #fff;}
      input{
        display: block;
        height: 40px;
        width: 97.2%;
        padding-left: 1em;
      }
      div{margin-bottom: 1em;}
      alternativa-md-textarea{margin-bottom: 1em;}
    </style>
    <form action="#">
      <h2>Edita Tu Perfil</h2>
      <div>
        <label for="name">Nombre:</label>
        <input type="text" id="name"/>
      </div>
      <div>
        <label for="email">Email:</label>
        <input type="email" id="email"/>
      </div>
      <div>
        <label for="name">Teléfono:</label>
        <input type="text" id="phone"/>
      </div>
      <div class="textareas">
        <label for="name">Leyenda:</label>
        <alternativa-md-textarea attrname="shortDesc">
          <alternativa-chars-counter chars="0" max="300"></alternativa-chars-counter>
        </alternativa-md-textarea>
      </div>
      <div class="textareas">
        <label for="name">Descripción:</label>
        <alternativa-md-textarea attrname="longDesc">
          <alternativa-chars-counter chars="0" max="10000"></alternativa-chars-counter>
        </alternativa-md-textarea>
      </div>
      <div>
        <button>Submit</button>
        <alternativa-message message=""></alternativa-message>
      </div>
    </form>
    `;
  }
  _getProductor() {
    document.addEventListener('DOMContentLoaded', () => {
      const event = new CustomEvent('retrieve-productor', {bubbles: true, detail: {id: this.id}});
      this.dispatchEvent(event);
    });
  }
  _fillForm(productor) {
    this.name.value = productor.name;
    this.email.value = productor.email;
    this.phone.value = productor.phone;
    this._shortDesc().editor.value(productor.shortDesc);
    this._longDesc().editor.value(productor.longDesc);
  }
  _eventsListener() {
    this.shadowRoot.querySelector('button').addEventListener('click', this._collectData.bind(this));
  }
  _collectData(event) {
    event.preventDefault();
    this.collect({_id: this.id});
    this.collect({name: this.name.value});
    this.collect({email: this.email.value});
    this.collect({phone: this.phone.value});
    this.collect({shortDesc: this._shortDesc().editor.value()});
    this.collect({longDesc: this._longDesc().editor.value()});
    this._createProductor();
  }

  _createProductor() {
    const event = new CustomEvent('edit-productor', {bubbles: true, detail: this.collected()});
    this.dispatchEvent(event);
  }
}

customElements.define('productor-edit', EditProductorForm );
