class ProductorForm extends Collecter{
  _addedWebcomponents() {
    this.addScriptTag("./js/components/form/alternativa-md-textarea.js");
    this.addScriptTag("./js/components/form/alternativa-progresbar.js");
    this.addScriptTag("./js/components/form/alternativa-chars-counter.js");
    this.addScriptTag("./js/components/form/alternativa-message.js");
    this.addScriptTag("./js/components/form/input-file.js");
    this.addScriptTag("./js/renderers/forms/point.js");
    this.addScriptTag("./js/renderers/forms/points-create.js");
  }
  _initialize() {
    this.name = this.shadowRoot.querySelector('[id="name"]');
    this.email = this.shadowRoot.querySelector('[id="email"]');
  }

  _cleanForm() {
    this.clear();
    this.name.value = '';
    this.email.value = '';
    this._description().editor.value('');
    this._cleanCharsCounter();
  }
  _cleanCharsCounter() {
    this._shortDesc().querySelector('alternativa-chars-counter').setAttribute('chars', "0");
  }
  _description() {
    return this.shadowRoot.querySelectorAll('alternativa-md-textarea')[0];
  }

}
