class CreateProductorForm extends ProductorForm{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._addedWebcomponents();
  }
  connectedCallback(){
    this._startWebComponent();
  }
  _startWebComponent() {
    this._template();
    this._initialize();

  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      form{width:70%; margin:0 auto; border: 1px solid #000; padding: 1em;}
      button{height:40px; padding: 0 2em;}
      alternativa-progresbar{width: 100;}
    </style>
    <form action="#">
      <alternativa-progresbar complete="33"></alternativa-progresbar>
      <productor-step1></productor-step1>
      <!-- <productor-step2></productor-step2>
      <productor-step3></productor-step3> -->
      <div>
        <button>Siguiente</button>
      </div>
    </form>
    `;
  }

  _eventsListener() {
    this.shadowRoot.querySelector('button').addEventListener('click', this._collectData.bind(this));
  }

  disconnectedCallback() {
    this.shadowRoot.querySelector('button').removeEventListener('click', this._collectData);
  }
  _collectData(event) {
    // this._createProductor();
  }

  _createProductor() {
    const event = new CustomEvent('create-productor', {bubbles: true, detail: this.collected()});
    this.dispatchEvent(event);
  }

}

customElements.define('productor-create', CreateProductorForm );
