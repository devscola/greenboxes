class EditDistributionPoints extends PointForm{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._addedWebcomponents();
    this.point = null;
  }
  connectedCallback(){
    this.point = JSON.parse(this.getAttribute('point'));
    this._template();
    this._addTags();
    this._addDays();
    this._eventsListener();
    this._initialize();
  }

  collectData(event) {
    if(event !== undefined) event.preventDefault();
    this.collect({_id: this.point._id});
    this._collectCommonData();
    this._submitPoint();
  }

  _titleContent() {
    return '<h2>Editar el punto de reparto</h2>'
  }

  _submitButton () {
    return '<button>Editar punto de reparto</button>'
  }

  _addTags() {
    this.shadowRoot.querySelector('alternativa-distribution-tags').setAttribute('tags', JSON.stringify(this.point));
  }

  _addDays() {
    this.shadowRoot.querySelectorAll('.days-check').forEach((element) => {
      this.point.days.forEach((day) => {
        if(element.value == day.day){
            element.checked = true;
            element.parentNode.children[0].value = day.hour;
            element.parentNode.children[0].disabled = false;
        }
      });
    });
  }

  _submitPoint() {
    const event = new CustomEvent('edit-distribution-point', {bubbles: true, detail: this.data});
    document.dispatchEvent(event);
    this.remove();
  }

}

customElements.define('points-edit', EditDistributionPoints );
