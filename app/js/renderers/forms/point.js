class PointForm extends Collecter{
  _addedWebcomponents() {
    this.addScriptTag("./js/components/form/alternativa-distribution-tags.js");
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      :host{
        max-width: 600px;
      }
      section{
        max-width: 600px;
        margin: 0 auto;
        padding: 1em;
        background-color: #3c9296 !important;
      }
      h2{
        font-size: 2em;
        color: #000;
        border-bottom: 1px solid #000;
      }
      label{color: #000;}
      input{
        display: block;
        height: 40px;
        width: 97.2%;
        margin-bottom: 1em;
        padding-left: 1em;
      }
      .days label, .in-place label{
        display: inline-block;
        min-width: 60px;
        text-align: center;
        margin-right:1em;
      }
      .hours{
        text-align: center;
        padding-left: 0;
        width: 57px;
        height: 15px;
      }
      alternativa-distribution-tags{
        background-color: #fff;
      }
    </style>
    <section>
      ${this._titleContent()}
      <div class="in-place">
        <label for="inPlace">
          A domicilio: <input type="checkbox" id="inPlace" ${this.point ? this.point.inPlace ? "checked" : "" : ""} />
        </label>
      </div>
      <div>
        <label for="place">Lugar:</label>
        <input type="text" id="place" value="${this.point ? this.point.place : ''}" />
      </div>
      <div>
        <label for="direction">Dirección:</label>
        <input type="text" id="direction" value="${this.point ? this.point.direction : ''}" />
      </div>
      <div class="days">
        <label for="days">Dias de reparto:</label>
        <br />
        <label for="monday">
          Lunes
          <input type="text" class="hours" placeholder="horario..." disabled id="mondayHour"/>
          <input type="checkbox" class="days-check" value="Lunes" id="monday"/>
        </label>
        <label for="tuesday">
          Martes
          <input type="text" class="hours" placeholder="horario..." disabled id="tuesdayHour"/>
          <input type="checkbox" class="days-check" value="Martes" id="tuesday"/>
        </label>
        <label for="wednesday">
          Miércoles
          <input type="text" class="hours" placeholder="horario..." disabled id="wednesdayHour"/>
          <input type="checkbox" class="days-check" value="Miércoles" id="wednesday"/>
        </label>
        <label for="thursday">
          Jueves
          <input type="text" class="hours" placeholder="horario..." disabled id="thursdayHour"/>
          <input type="checkbox" class="days-check" value="Jueves" id="thursday"/>
        </label>
        <label for="friday">
          Viernes
          <input type="text" class="hours" placeholder="horario..." disabled id="fridayHour"/>
          <input type="checkbox" class="days-check" value="Viernes" id="friday"/>
        </label>
        <label for="saturday">
          Sabado
          <input type="text" class="hours" placeholder="horario..." disabled id="saturdayHour"/>
          <input type="checkbox" class="days-check" value="Sabado" id="saturday"/>
        </label>
        <label for="sunday">
          Domingo
          <input type="text" class="hours" placeholder="horario..." disabled id="sundayHour"/>
          <input type="checkbox" class="days-check" value="Domingo" id="sunday"/>
        </label>
      </div>
      <div class="tags">
        <label>Tags de búsqueda:</label>
        <alternativa-distribution-tags></alternativa-distribution-tags>
      </div>
      <div>
        ${this._submitButton()}
      </div>
      <div>
        <ul></ul>
      </div>
    </section>
    `;
  }
  _initialize() {
    this.inPlace = this.shadowRoot.querySelector('[id="inPlace"]');
    this.place = this.shadowRoot.querySelector('[id="place"]');
    this.direction = this.shadowRoot.querySelector('[id="direction"]');
  }

  _collectCommonData () {
    this.collect({inPlace: this.inPlace.checked});
    this.collect({place: this.place.value});
    this.collect({direction: this.direction.value});
    this.collect({days: this._collectDays()});
    this._collectTags();
  }

  _collectDays() {
    let days = [];
    this.shadowRoot.querySelectorAll('.days input').forEach((element) => {
      if(element.checked) {
        let day = {};
        day.day = element.value;
        day.hour = element.parentNode.children[0].value;
        days.push(day);
      }
    });
    return days;
  }

  _collectTags() {
    this.shadowRoot.querySelector('alternativa-distribution-tags').setAttribute('collect', true);
  }

  _collectedTags(event) {
    this.collect({tags: event.detail.tags});
  }

  _focusedHours(event) {
    let element = event.target.parentNode.children[0];
    if(event.target.checked){
      element.disabled = false;
      element.focus();
    }else{
      element.disabled = true;
      element.value = '';
    }
  }

  _eventsListener() {
    this.shadowRoot.querySelector('button').addEventListener('click', this.collectData.bind(this));
    this.shadowRoot.addEventListener('collected-tags', this._collectedTags.bind(this));
    this.shadowRoot.querySelectorAll('.days-check').forEach(element => {
      element.addEventListener('click', this._focusedHours.bind(this));
    });
  }

  disconnectedCallback() {
    this.shadowRoot.querySelector('button').removeEventListener('click', this.collectData);
    this.shadowRoot.removeEventListener('collected-tags', this._collectedTags);
    this.shadowRoot.querySelectorAll('.days-check').forEach(element => {
      element.removeEventListener('click', this._focusedHours);
    });
  }
}
