class Collecter extends Scripter{
  constructor(){
    super();
    this.data = {};
  }
  collect(value) {
    this.data = Object.assign(this.data, value);
  }
  collected() {
    return this.data;
  }
  clear() {
    this.data = {};
  }
}
