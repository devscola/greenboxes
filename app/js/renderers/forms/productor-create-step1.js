class ProductorStep1 extends ProductorForm{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._template();
  }
  connectedCallback() {
    this._eventsListener();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      label{width:100%;}
      input{width: 100%;}
      div{margin:1em 0;}
      alternativa-md-textarea{margin-bottom: 1em;}
    </style>
    <h2>Datos Personales</h2>
    <div>
      <label for="name">Nombre:</label>
      <input type="text" id="name"/>
    </div>
    <div>
      <label for="email">Email:</label>
      <input type="email" id="email"/>
    </div>
    <input-file></input-file>
    <div class="textareas">
      <label for="description">Descripción:</label>
      <alternativa-md-textarea attrname="description">
        <alternativa-chars-counter chars="0" max="10000"></alternativa-chars-counter>
      </alternativa-md-textarea>
    </div>

    `;
  }

  _collectData(event) {
    event.preventDefault();
    this.collect({name: this.name.value});
    this.collect({email: this.email.value});
    this.collect({description: this._shortDesc().editor.value()});
  }


}

customElements.define('productor-step1', ProductorStep1 );
