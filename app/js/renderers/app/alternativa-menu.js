class Menu extends HTMLElement{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._template();
  }
  connectedCallback(){
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      ul{list-style: none; background-color:#000;}
      li{display:inline-block; margin: 1em;}
      a{color: #fff; text-decoration:none;}
    </style>
    <nav>
      <ul>
        <li><a href="crear-productor.html">Registrate</a></li>
        <li><a href="listar-productores.html">Lista de productores</a></li>
      </ul>
    </nav>
    `;
  }
  disconnectedCallback() {
  }
}

customElements.define('alternativa-menu', Menu );
