class Scripter extends HTMLElement{
  constructor(){
    super();
  }
  addScriptTag(src){
    let script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    document.querySelector('head').append(script);
  }
}
