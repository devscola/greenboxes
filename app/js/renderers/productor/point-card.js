class DistributionPoint extends Scripter{
  static get observedAttributes() { return ['point']; }

  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this.point = null;
  }
  connectedCallback(){
    this.point = JSON.parse(this.getAttribute('point'));
    this._template();
    this._addDays();
    this._eventsListener();
  }
  attributeChangedCallback(name, oldValue, newValue) {
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      article{
        position:relative;
        display:inline-block;
        width: 360px;
        margin: 0 auto;
        padding: 1em;
        background-color: #3c9296;
        color: #000;
      }
      div:first-child{
        position: absolute;
        top: 15px;
        right: 15px;
      }
      h2{
        color: #000;
        border-bottom: 1px solid #000;
      }
    </style>
    <article>
      <div><button id="edit">Editar</button><button>Borrar</button></div>
      <h2>${this.point.inPlace ? "Reparto a domicilio" : this.point.place}</h2>
      <div>
        <h3>${this.point.direction}</h3>
        <div id="days"></div>
        <p>Busquedas: ${this.point.tags.join()}</p>
      </div>
    </article>
    `;
  }

  _eventsListener() {
    this.shadowRoot.querySelector('#edit').addEventListener('click', () => {
      const event = new CustomEvent('edit-point', {bubbles: true, detail: this.point});
      this.dispatchEvent(event);
    })
  }

  _addDays() {
    this.point.days.forEach((element) => {
      let tag = document.createElement('p');
      tag.innerHTML = "Dia: " + element.day + " - Horario: " + element.hour;
      this.shadowRoot.querySelector("#days").append(tag);
    });
  }

  disconnectedCallback() {
  }
}

customElements.define('point-card', DistributionPoint );
