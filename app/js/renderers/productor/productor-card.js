class ProductorCard extends Scripter{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._addedWebcomponents();
    this.productor = null;
    this.converter = new showdown.Converter();
  }
  connectedCallback(){
    if(this.hasAttribute('productor') && this.getAttribute('productor') != null){
      this.productor = JSON.parse(this.getAttribute('productor'));
      this._template();
      this.shadowRoot.querySelector('alternativa-distribution').setAttribute('productor', this.getAttribute('productor'))
      this._eventsListener();
    }
  }
  _addedWebcomponents() {
    this.addScriptTag("./js/renderers/productor/alternativa-distribution.js");
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
    .legend{font-weight: bolder;}
    section{
      width: 100%;
      margin: 0 auto;
      padding: 1em;
      padding-bottom: 2em;
      background-color: #3c9296;
    }
    h1{
      border-bottom: 1px solid #fff;
    }
    img{max-width:100%;}
    div{margin-bottom: 1em; color: #fff;}
    section{position:relative;}
    section>div:first-child{position:absolute; right:0}
    button{margin-right: 1em;}
    </style>
    <section>
      <div>
        <button>Editar</button>
        <button>Borrar perfil</button>
      </div>
      <h1>
      ${this.productor.name}
      </h1>
      <div>
        <p>Email: ${this.productor.email}</p>
        <p>Teléfono: ${this.productor.phone}</p>
      </div>
      <div class="legend">
        ${this.converter.makeHtml(this.productor.shortDesc)}
      </div>
      <hr>
      <div>
        ${this.converter.makeHtml(this.productor.longDesc)}
      </div>
      <div>
        <alternativa-distribution></alternativa-distribution>
      </div>
    </section>
    `;
  }
  _eventsListener() {
    this.shadowRoot.querySelectorAll('button')[0].addEventListener('click', this._goToEdit.bind(this));
    this.shadowRoot.querySelectorAll('button')[1].addEventListener('click', this._deleteAcount.bind(this));
  }
  _deleteAcount() {
    if(confirm('Quieres anular tu cuenta de productor?')){
      const event = new CustomEvent('destroy-productor', {bubbles: true, detail: {id: this.productor._id}});
      this.dispatchEvent(event);
    }
  }
  _goToEdit() {
    if(window.location.href.includes('editar-productor')){
      window.location.reload();
    }else{
      window.location.href = `editar-productor.html#${this.productor._id}`;
    }
  }
  disconnectedCallback() {
    this.shadowRoot.querySelectorAll('button')[0].removeEventListener('click', this._deleteAcount);
    this.shadowRoot.querySelectorAll('button')[1].removeEventListener('click', this._goToEdit);
  }
}

customElements.define('productor-card', ProductorCard );
