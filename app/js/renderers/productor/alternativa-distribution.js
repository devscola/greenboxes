class Distribution extends Scripter{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._addedWebcomponents();
    this._template();
    this.productor = null;
  }
  connectedCallback(){
    this.productor = JSON.parse(this.getAttribute('productor'));
    this.productor.distribution.forEach(element => {
      let component = document.createElement('point-card');
      component.setAttribute('point', JSON.stringify(element));
      this.shadowRoot.querySelector('section').append(component);
    });
    this._eventsListener();
  }
  _addedWebcomponents() {
    this.addScriptTag("./js/renderers/productor/point-card.js");
  }
  attributeChangedCallback(name, oldValue, newValue) {
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      section{
        width: 100%;
        margin: 0 auto;
        padding: 1em;
        background-color: #fff !important;
      }
      h2{
        font-size: 2em;
        color: #000;
        border-bottom: 1px solid #000;
      }
    </style>
    <section>
      <h2>Puntos de reparto</h2>
      <div></div>
    </section>
    `;
  }

  _eventsListener() {
    this.shadowRoot.addEventListener('edit-point', (event) => {
      let component = document.createElement('points-edit');
      component.setAttribute('point', JSON.stringify(event.detail));
      this.shadowRoot.querySelector('div').append(component);
    })
  }

  disconnectedCallback() {
  }
}

customElements.define('alternativa-distribution', Distribution );
