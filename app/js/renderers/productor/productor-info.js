class Info extends HTMLElement{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
  }
  connectedCallback(){
    this.id = window.location.href.split("#")[1];
    if(this.id != 'undefined'){
      this._getProductor();
    }
  }
  _getProductor(){
    document.addEventListener('DOMContentLoaded', () => {
      const event = new CustomEvent('info-productor', {bubbles: true, detail: {id: this.id}});
      this.dispatchEvent(event);
    });
  }
  disconnectedCallback() {
  }
}

customElements.define('productor-info', Info );
