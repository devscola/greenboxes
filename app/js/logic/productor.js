class Productor{
  constructor(bus) {
    this.bus = bus;
    this._eventsListener();
    this._subscriptions();
  }

  _stored(response){
    document.querySelector('productor-edit').remove();
    const productor = response.productor;
    window.location.href = `info-productor.html#${productor._id}`;
  }

  _retrieved(response){
    const productor = response.productor;
    let component = document.querySelector('productor-edit');
    component.setAttribute("productor", JSON.stringify(productor));
  }

  _retrievedInfo(response){
    document.querySelector('productor-info').remove();
    const productor = response.productor;
    let component = document.createElement("productor-card");
    component.setAttribute("productor", JSON.stringify(productor));
    document.querySelector('body').append(component);
  }

  _retrievedAll(response){
    const productors = response.productors;
    let component = document.querySelector('alternativa-list');
    component.setAttribute("productors", JSON.stringify(productors));
  }

  _destroied(response){
    document.querySelector('productor-card').remove();
    const message = response.message;
    let component = document.createElement("alternativa-message");
    component.setAttribute("message", message);
    document.querySelector('body').append(component);
  }


  _eventsListener() {
    document.addEventListener('create-productor', (event) => {
      const payload = event.detail;
      this.bus.publish('productor.create', payload);
    });
    document.addEventListener('edit-productor', (event) => {
      const payload = event.detail;
      this.bus.publish('productor.update', payload);
    });
    document.addEventListener('retrieve-productor', (event) => {
      const payload = event.detail;
      payload.action = 'productor.retrieved';
      this.bus.publish('productor.retrieve', payload);
    });
    document.addEventListener('info-productor', (event) => {
      const payload = event.detail;
      payload.action = 'productor.retrieved.info';
      this.bus.publish('productor.retrieve', payload);
    });
    document.addEventListener('retrieve-all', (event) => {
      const payload = {};
      this.bus.publish('productor.retrieve.all', payload);
    });
    document.addEventListener('destroy-productor', (event) => {
      const payload = event.detail;
      this.bus.publish('productor.destroy', payload);
    });
    document.addEventListener('edit-distribution-point', (event) => {
      const payload = event.detail;
      this.bus.publish('productor.distribution.update', payload);
    });
  }

  _subscriptions() {
    this.bus.subscribe('productor.stored', this._stored.bind(this));
    this.bus.subscribe('productor.retrieved', this._retrieved.bind(this));
    this.bus.subscribe('productor.destroied', this._destroied.bind(this));
    this.bus.subscribe('productor.retrieved.info', this._retrievedInfo.bind(this));
    this.bus.subscribe('productor.retrieved.all', this._retrievedAll.bind(this));
  }
}
