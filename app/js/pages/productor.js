class ProductorPage{
  constructor() {
    this.bus = new Bus();
    new ProductorService(this.bus);
    new Productor(this.bus);
  }
}
