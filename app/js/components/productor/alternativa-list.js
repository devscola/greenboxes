class List extends HTMLElement{
  static get observedAttributes() { return ['productors']; }
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this.productors = [];
    this._template();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style></style>
    <section></section>
    `;
  }
  connectedCallback(){
    if(this.hasAttribute('productors')){
      this._parseJSON(this.getAttribute('productors'));
      this._showItems();
    }
    this._eventsListener();
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this._parseJSON(newValue);
    this._showItems();
  }

  _eventsListener() {
    document.addEventListener('DOMContentLoaded', () => {
      const event = new CustomEvent('retrieve-all', {bubbles: true});
      this.dispatchEvent(event);
    });
  }

  _showItems() {
    for(let i = 0 ; i < this.productors.length ; i++){
      let item = document.createElement("alternativa-list-item");
      item.setAttribute("productor", JSON.stringify(this.productors[i]));
      this.shadowRoot.querySelector('section').append(item);
    }
  }

  _parseJSON(productors) {
    this.productors = JSON.parse(productors);
  }

}

customElements.define('alternativa-list', List );
