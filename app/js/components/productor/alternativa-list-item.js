class ListItem extends HTMLElement{
  static get observedAttributes() { return ['productor']; }
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this.converter = new showdown.Converter();
    this._template();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style></style>
    <article><h3></h3><div></div></article>
    `;
  }
  connectedCallback(){
    if(this.hasAttribute('productor')){
      this._addProductor(JSON.parse(this.getAttribute('productor')));
    }
  }

  attributeChangedCallback(name, oldValue, newValue) {
      this._addProductor(JSON.parse(newValue));
  }

  _addProductor(productor) {
    this.shadowRoot.querySelector('h3').innerHTML = productor.name + this._showCardInfoButton(productor._id);
    this.shadowRoot.querySelector('div').innerHTML = this.converter.makeHtml(productor.shortDesc);
  }

  _showCardInfoButton(id) {
    return ` <a href="${'info-productor.html#'+id}">Ver...</a>`;
  }

}

customElements.define('alternativa-list-item', ListItem );
