class Message extends HTMLElement{
  static get observedAttributes() { return ['message']; }
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._template();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>p{font-size:1.5em;}:host{color:#000;}</style>
    <div><p></p></div>
    `;
  }
  connectedCallback(){
    if(this.hasAttribute('message')){
      this.shadowRoot.querySelector('p').innerText = this.getAttribute('message');
    }
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if(this.hasAttribute('message')){
      this.shadowRoot.querySelector('p').innerText = newValue;
      const event = new CustomEvent('clear-productor', {bubbles: true});
      this.dispatchEvent(event);
    }
  }

}

customElements.define('alternativa-message', Message );
