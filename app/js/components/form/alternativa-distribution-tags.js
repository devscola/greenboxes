class TagsList extends Scripter{
  static get observedAttributes() { return ['collect', 'tags']; }
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._addedWebcomponents();
    this._template();
    this.tags = [];
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      section{
        position:relative;
        border: 1px solid darkgrey;
        height:auto;
      }
      div{
        position:relative;
        top:0;
        height: auto;
        width:auto;
        padding:.5em;
        background-color: #fff;
        z-index: 1;
      }
      input{
        position:relative;
        bottom:0;
        border:none;
        height: 28px;
        width: 97.4%;
        padding-left: 1em;
      }
      input:focus{
        outline: none;
      }
      alternativa-tag{
        display: inline-block;
      }
    </style>
    <section>
      <div></div>
      <input type="text" placeholder="La Cova, Benimaclet"/>
    </section>
    `;
  }
  connectedCallback(){
    this._eventsListener();
  }

  attributeChangedCallback(name, oldValue, newValue) {
    let point = JSON.parse(newValue);
    if(name === 'collect'){
      if(newValue == "true"){
        this._collectTags();
      }else if(newValue == 'false'){
        this._clearTags();
      }
    }else if(name === 'tags'){
      if(point.tags !== undefined){
        point.tags.forEach((tag) => {
          this._createTag(tag);
        });
      }
    }
  }

  _collectTags() {
    this.shadowRoot.querySelectorAll('alternativa-tag').forEach(element => {
      this.tags.push(element.getAttribute('content'));
    });
    const event = new CustomEvent('collected-tags', {bubbles: true, detail: {tags: this.tags}});
    this.dispatchEvent(event);
  }

  _clearTags() {
    this.tags = [];
    this.shadowRoot.querySelectorAll('alternativa-tag').forEach(element => {
      element.remove();
    });
  }

  _addedWebcomponents() {
    this.addScriptTag("./js/components/form/alternativa-tag.js");
  }

  _eventsListener() {
    this.shadowRoot.querySelector('section').addEventListener('click', this._focusInInput.bind(this));
    this.shadowRoot.querySelector('input').addEventListener('keyup', this._addTag.bind(this));
  }

  _addTag(event) {
    const key = event.keyCode;
    if(key == 13 || key == 188){
      event.target.setAttribute('placeholder', '');
      let value = event.target.value;
      if(key == 188) value = value.slice(0, -1);
      this._createTag(value);
      event.target.value = '';
    }
  }

  _createTag(value){
    let component = document.createElement("alternativa-tag");
    component.setAttribute('content', value);
    this.shadowRoot.querySelector('div').append(component);
  }

  _focusInInput() {
    this.shadowRoot.querySelector('input').focus();
  }

  disconnectedCallback() {
    this.shadowRoot.querySelector('section').removeEventListener('click', this._focusInInput);
    this.shadowRoot.querySelector('input').removeEventListener('keyup', this._addTag);
  }

}

customElements.define('alternativa-distribution-tags', TagsList );
