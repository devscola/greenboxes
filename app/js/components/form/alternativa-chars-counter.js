class CharsCounter extends HTMLElement{
  static get observedAttributes() { return ['chars']; }
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._template();
    this.counter = this.getAttribute('max');
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>div{display:inline-block;font-size:1.3em;}:host{color:#fff;}</style>
    <div>Quedan: <span></span></div>
    `;
  }
  connectedCallback(){
    this.shadowRoot.querySelector('span').innerText = this.getAttribute('max');
  }

  attributeChangedCallback(name, oldValue, newValue) {
     this._updateCharsCounter(newValue);
     this._limitedCharsEvents();
  }
  _updateCharsCounter(newValue) {
    if(this.getAttribute('chars')){
      this.counter = this.getAttribute('max') - newValue;
      this.shadowRoot.querySelector('span').innerText = this.counter;
    }
  }
  _limitedCharsEvents() {
    if(this.counter < 0){
      const event = new CustomEvent('limited-chars', {bubbles: true});
      this.dispatchEvent(event);
      this.shadowRoot.querySelector('span').style.color = 'red';
    }else{
      const event = new CustomEvent('limited-chars-ok', {bubbles: true});
      this.dispatchEvent(event);
      this.shadowRoot.querySelector('span').style.color = 'black';
    }
  }
}

customElements.define('alternativa-chars-counter', CharsCounter );
