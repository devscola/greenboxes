class Tag extends HTMLElement{
  static get observedAttributes() { return ['content']; }

  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._template();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      :host{
        margin: .1em;
        margin-bottom:.3em;
      }
      div{
        position:relative;
        display: inline-block;
        color: #fff;
        padding: .4em .4em;
        padding-right:1.4em;
        background-color: #3c9296;
      }
      span{
        position:absolute;
        cursor:pointer;
        top:0;
        right:0;
        background-color: red;
        padding: 0 .3em;
        color: #fff;
        font-size: 1em;
        font-weight: bolder;
      }
    </style>
    <div><span>x</span></div>
    `;
  }
  connectedCallback(){
    this._eventsListener();
  }

  _eventsListener() {
    this.shadowRoot.querySelector('span').addEventListener('click', this._removeTag.bind(this));
  }

  _removeTag() {
    this.remove();
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.shadowRoot.querySelector('div').prepend(newValue);
  }

  disconnectedCallback() {
    this.shadowRoot.querySelector('span').removeEventListener('click', this._removeTag);
  }
}

customElements.define('alternativa-tag', Tag );
