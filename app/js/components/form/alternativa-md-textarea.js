class MdTextArea extends HTMLElement{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this.editor = null;
    this.chars = 0;
    this._template();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      slot{display:block; text-align:right;}
      .CodeMirror,.CodeMirror-scroll {min-height: 100px !important;}
      .editor-toolbar{background-color: #fff;}
    </style>
    <textarea></textarea>
    <slot></slot>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
    `;
  }
  connectedCallback(){
    const name = this.getAttribute("attrname");
    this.shadowRoot.querySelector('textarea').setAttribute('name', name);
    this._attachMarkdownEditor();
    this._eventsListener();
  }
  _attachMarkdownEditor(){
    this.editor = new SimpleMDE({ element:this.shadowRoot.querySelector('textarea'), status: false, spellChecker: false});
    this.editor.codemirror.on("change", () => {
    	this._updateCharsToolbarEvent();
    });
  }

  _eventsListener() {
    this.shadowRoot.addEventListener('limited-chars', this._limitedCharsEvent.bind(this));
    this.shadowRoot.addEventListener('limited-chars-ok', this._limitedCharsOkEvent.bind(this));
  }
  _updateCharsToolbarEvent() {
    this.chars = this._charsSize();
    this._charsCounter().setAttribute('chars', this.chars);
  }
  _limitedCharsEvent(){
    this._simpleMDBox().style.border = '2px solid red';
  }
  _limitedCharsOkEvent(){
    this._simpleMDBox().style.border = '1px solid #ddd';
  }
  _charsCounter() {
    return this.shadowRoot.querySelector('slot').assignedElements()[0];
  }
  _charsSize() {
    return this.editor.value().length;
  }
  _simpleMDBox() {
    return this.shadowRoot.querySelector('.CodeMirror');
  }

  disconnectedCallback() {
      this.shadowRoot.removeEventListener('keydown', this._deleteCharsEvent);
      this.shadowRoot.removeEventListener('limited-chars', this._limitedCharsEvent);
      this.shadowRoot.removeEventListener('limited-chars-ok', this._limitedCharsOkEvent);
      this.shadowRoot.removeEventListener('input', this._updateCharsToolbarEvent);
      this.shadowRoot.querySelector('.editor-toolbar').removeEventListener('click', this._updateCharsToolbarEvent);
  }
}

customElements.define('alternativa-md-textarea', MdTextArea );
