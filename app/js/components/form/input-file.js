class InputFile extends ProductorForm{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._template();
  }

  connectedCallback () {
    this._eventsListener();
  }

  _template () {
    this.shadowRoot.innerHTML = `
    <style>
      .file > label, .file > input{opacity: 0;}
      .file {display:inline-block;background-color: #ccc; width:10em; height: 10em; border-radius: 50%;}
      .file > span{font-weight: bolder;position: relative;top: -0.08em; left:.2em;font-size:10em; color: #fff}
    </style>
      <div class="file">
        <span>+</span>
        <label for="image">Foto:</label>
        <input type="file" id="image" />
      </div>
    `
  }

  _eventsListener () {
    this.shadowRoot.querySelector('.file').addEventListener('click', (event) => {
      event.target.parentNode.children[2].click()
    })
  }
}
customElements.define('input-file', InputFile );
