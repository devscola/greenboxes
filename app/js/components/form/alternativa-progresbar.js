class Progresbar extends HTMLElement{
  constructor(){
    super();
    const shadowRoot = this.attachShadow({ mode:'open' });
    this._template();
  }
  _template(){
    this.shadowRoot.innerHTML=`
    <style>
      div{position:relative;}
      span{position:absolute; z-index:2; padding: .36em .5em;}
      progress{height:30px; width:100%; z-index:1;}
    </style>
    <div>
      <span></span>
      <progress value="0" max="100"></progress>
    </div>
    `;
  }
  connectedCallback(){
    if(this.hasAttribute('complete')){
      this._changeState()
    }
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if(this.hasAttribute('complete')){
      this._changeState()
    }
  }

  _changeState () {
    this.shadowRoot.querySelector('span').textContent=`Llevas un ${this.getAttribute('complete')}%`
    this.shadowRoot.querySelector('progress').value = this.getAttribute('complete')
  }

}

customElements.define('alternativa-progresbar', Progresbar );
