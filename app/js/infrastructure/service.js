class Service {
  constructor(bus) {
    this.bus = bus;
  }
  buildCallback(signal) {
    return response => {
      this.bus.publish(signal, response);
    };
  }
}
