class Bus {
  constructor() {
    this.subscriptions = {};
  }
  subscribe(signal, action){
    if (!this._isset(signal)) {
      this._init(signal);
    }
    this._add(signal, action);
  }

  publish(signal, action){
    if (this._isset(signal)) {
      this._fire(signal, action);
    }
  }

  _isset(signal) {
    return (this.subscriptions[signal] != undefined);
  }

  _fire(signal, action) {
    this.subscriptions[signal].forEach((handle) => {
      handle(action);
    });
  }

  _init(signal) {
    this.subscriptions[signal] = [];
  }

  _add(signal, action) {
    this.subscriptions[signal].push(action);
  }
}
