class Client{
  static async doRequest(endpoint, payload, callback){
    const baseUri = Client._baseUri()
    fetch(baseUri + endpoint, {
      mode: 'cors',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload)
    }).then(response => {
      return response.json();
    }).then(result => {
      console.log(result);
      callback(result);
    }).catch(err => console.log(err));
  }

  static _baseUri () {
    if (window.location.href.includes('localhost')
        || window.location.href.includes('file')) return 'http://localhost:3001'
    if (window.location.href.includes('heroku')) return 'https://greenboxes-api.herokuapp.com'
    return 'https://greenboxes-api.herokuapp.com'
  }
}
